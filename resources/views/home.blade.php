<!-- Using the Layout Menu -->
@extends('layouts.menu')

@section('title')
N.D.Serveis
@endsection

@section('content')
<div id="main">
  <div id="carouselExampleIndicators" class="carousel slide carouselDiv" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img class="d-block w-100" src="./images/imagen1.png" alt="First slide">
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="./images/imagen2.jpg" alt="Second slide">
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="./images/imagen3.jpg" alt="Third slide">
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>
<div class="ourServices">
  <h3>Nuestros Servicios</h3>
</div>
<div class="rectangleRedFather">
  <div class="rectangleRedDaughter"></div>
</div>
<div class="divRowCircles">
  <div class="col1 col-xs-12">
    <div class="borderCircle">
      <img id="redHouse" src="./images/variousImages/redhouse.png" alt="redHouse.png">
    </div>
    <div class="rectangleRedFather2">
      <h6 class="divTextRectangle">REFORMAS</h6>
      <div class="rectangleRedDaughter2"></div>
      <p class="textInfoServices">Domicilios - Comunidades <br>- Locales y Oficinas</p>
    </div>
  </div>
  <div class="col2 col-xs-12">
    <div class="borderCircle">
      <img id="redPaper" src="./images/variousImages/redPaper.png" alt="redPaper.png">
    </div>
    <div class="rectangleRedFather2_1">
      <h6 class="divTextRectangle">LEGALIZACIONES</h6>
      <div class="rectangleRedDaughter2"></div>
      <p class="textInfoServices">Más Información<br>Consulta nuestra página</p>
    </div>
  </div>
  <div class="col3 col-xs-12">
    <div class="borderCircle">
      <img id="redWater" src="./images/variousImages/redWater.png" alt="redWater.png">
    </div>
    <div class="rectangleRedFather2_2">
      <h6 class="divTextRectangle">INSTALACIONES</h6>
      <div class="rectangleRedDaughter2"></div>
      <p class="textInfoServices">Agua, gas y electricidad - <br>Calefacción y aire acondicionado</p>
    </div>
  </div>
  <div class="col4 col-xs-12">
    <div class="borderCircle">
      <img id="redGear" src="./images/variousImages/redGear.png" alt="redGear.png">
    </div>
    <div class="rectangleRedFather2_3">
      <h6 class="divTextRectangle">OTROS SERVICIOS</h6>
      <div class="rectangleRedDaughter2"></div>
      <p class="textInfoServices">Más Información<br>Consulta nuestra página</p>
    </div>
  </div>
</div>
@endsection
