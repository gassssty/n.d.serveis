$(document).ready(function(){
	/* NavBar Actions */
	const toggler = document.querySelector('.navbar > .toggler'),
  navListContainer = document.querySelector('.navbar > .nav-list-container');
	toggler.addEventListener(
	  "click",
	  () => {
	    //convert hamburger to close
	    toggler.classList.toggle('cross');
	    //make nav visible
	    navListContainer.classList.toggle('nav-active');
	  },
	  true
	);

	/* SearchNav Actions */
	$(".btn").click(function(){
  	$(".input").toggleClass("active").focus;
  	$(this).toggleClass("animate");
  	$(".input").val("");
	});

	$(window).scroll(function(){
        if ($(this).scrollTop() > 100) {
            $('#scroll').fadeIn();
        } else {
            $('#scroll').fadeOut();
        }
    });
    $('#scroll').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });
});
