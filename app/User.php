<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {
    use Notifiable;

    //To disable the timestat at the DataBase
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'username','name','surnames',
      'user_photo','password',
      'date_birth','email','phone',
      'country_id', 'role_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
      'password',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The role of the User logged.
     *
     * @var array
     */
    public function role() {
      return $this->belongsTo('App\Role');
    }

    /**
     * Returns if its admin or not
     *
     * @var array
     */
    public function isAdmin() {
      if ($this->role->name_role == 'admin') {
        return true;
      }
      return false;
    }
}
