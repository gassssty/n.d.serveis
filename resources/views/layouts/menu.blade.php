<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>@yield('title')</title>
  <link rel="stylesheet" href="vendors/css/bootstrap.min.css">
  <link rel="icon" href="{!! asset('images/ndserveis.png') !!}"/>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

  <!-- Edited CSS -->
  <link rel="stylesheet" href="css/styles.css">
  <link rel="stylesheet" href="css/searchNav.css">
  <link rel="stylesheet" href="css/buttonSUP.css">
  <link rel="stylesheet" href="css/carousel.css">
  <link rel="stylesheet" href="css/login.css">
  <link rel="stylesheet" href="css/infoServices.css">
  <link rel="stylesheet" href="css/formReview.css">

  <!-- Links of SCRIPTS and JQuery and BootStrap-->
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/scripts.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

  <!-- TipoGrafies and Fonts -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,wght@0,300;0,700;1,400&display=swap" rel="stylesheet">
  <!-- font-family: 'Pontano Sans', sans-serif; THE PAGE -->
  <link href="https://fonts.googleapis.com/css2?family=Pontano+Sans&display=swap" rel="stylesheet">
  <!-- font-family: 'Nunito Sans', sans-serif; TITLES OR SUBTITLES -->
</head>
<body>
  <!-- Button of SCROLL UP -->
  <a href="#" id="scroll"><span></span></a>
  <div class="containerDiv">
    <div class="firstDivHeader">
      <div class="divHeader1">
        <h1><a class="titleLogo" href="/">N.D.Serveis</a></h1>
      </div>
      <div class="divHeader2">
        <p class="topRightContent">
          <h3 class="telephoneNumber">666666666</h3>
          &nbsp;
          <img id="logoWhatsAppRed" src="images/whatsapp_red.png" alt="whatsapp_red.png">
          &nbsp;
          <a class="aTextDecoration" href="#">CA</a> <p class="aTextDecorationBar">|</p> <a class="aTextDecoration" href="#">ES</a>
        </p>
      </div>
    </div>
  </div>
  <div class="containerDiv2">
    <!-- Navbar -->
    <nav class="navbar">
      <!-- Toggler -->
      <button class="toggler displayInBlock">
        <span class="line"></span>
        <span class="line"></span>
        <span class="line"></span>
      </button>
      <div class="nav-list-container secondDivHeader">
        <ul class="nav-list">
          <li class="displayInBlock"><a href="/">Inicio</a></li>
          <li class="displayInBlock"><a href="#">Agua</a></li>
          <li class="displayInBlock"><a href="#">Gas</a></li>
          <li class="displayInBlock"><a href="#">Luz</a></li>
          <li class="displayInBlock"><a href="#">Contacto</a></li>
          @guest
          <li class="displayInBlock logoutLoginButton2"><a href="{{route('login')}}">LOGIN</a></li>
          @if (Route::has('register'))
          <li class="displayInBlock logoutLoginButton2"><a href="{{route('register')}}">REGISTER</a></li>
          @endif
          @else
          <li class="displayInBlock logoutLoginButton2"><a href="{{route('register')}}"
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">LOGOUT</a></li>
            <form id="logout-form" action="{{route('logout')}}" method="POST" style="display: none;">
              @csrf
            </form>
          @endguest
        </ul>
        <span id="spanHeader1">
          <ul class="nav-list2">
            @guest
            <li class="displayInBlock"><a href="{{route('login')}}">LOGIN</a></li>
            @if (Route::has('register'))
            <li class="displayInBlock"><a href="{{route('register')}}">REGISTER</a></li>
            @endif
            @else
            <li class="displayInBlock logoutButton"><a href="{{route('register')}}"
              onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">LOGOUT</a></li>
              <form id="logout-form" action="{{route('logout')}}" method="POST" style="display: none;">
                @csrf
              </form>
            @endguest
            </ul>
            <div class="search-box">
              <input class="search-txt" type="text" name="" placeholder="Type to Search">
              <a class="search-btn" href="#">
                <i class="fas fa-search"></i>
              </a>
            </div>
          </span>
        </div>
      </nav>
    </div>
    <h1><a class="titleLogoSlideBar" href="/">N.D.Serveis</a></h1>
    <main class="container">
      @yield('content')
      <!--@include('layouts.forms.review')-->
    </main>
    <!--<footer class="textCenterDiv">
      <div class="phoneText">
        <p>Teléfono: 666666666</p>
      </div>
      <div class="copyrightText">
        <p>© 2021 N.D.Serveis All rights reserved.</p>
      </div>
      <div class="addressText">
        <p>C\Berlin n73</p>
      </div>
    </footer>-->
  </body>
