<br><br><br><br><br><br><br>
<div class="text-center dropdown-content-review">
  <h1 id="titleReview">Add a Review</h1>
  <div class="review-form-1">
    <form method="POST" action="POST" class="text-left">
      @csrf
      <div class="review-form-main-message"></div>
      <div class="main-review-form">
        <div class="review-group">
          <div class="form-group">
            <label for="name" class="sr-only">Your Name</label>
            <input type="text" class="form-control @error('name') is-invalid @enderror"
            required name="name" placeholder="Your Name"
            value="{{old('name')}}" id="name" autofocus>
            @error('name')
            <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
            </span>
            @enderror
          </div>
          <div class="form-group">
            <label for="description" class="sr-only">Description</label>
            <input type="text" name="description"
            placeholder="Put the Description" class="form-control">
          </div>
          <div class="form-group">

          </div>
        </div>
        <button type="submit" class="review-button"><i class="fa fa-chevron-right"></i></button>
      </div>
    </form>
  </div>
</div>
