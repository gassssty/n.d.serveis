<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {
  /**
  * Run the migrations to create the Table in the DATABASE.
  *
  * @return void
  */
  public function up() {
    Schema::create('users', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('username');
      $table->string('name');
      $table->string('surnames');
      $table->integer('role_id');
      $table->string('user_photo')->nullable();
      $table->string('password');
      $table->date('date_birth');
      $table->string('email')->unique();
      $table->integer('phone');
      $table->integer('country_id');
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down() {
    Schema::dropIfExists('users');
  }
}
