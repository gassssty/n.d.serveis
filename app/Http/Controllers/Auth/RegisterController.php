<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Get a validator for an incoming registration request by te user.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data) {
        return Validator::make($data, [
          'username' => ['required', 'string', 'max:20'],
          'name' => ['required', 'string', 'max:50'],
          'surnames' => ['required', 'string', 'max:50'],
          'password' => ['required', 'string', 'min:8', 'confirmed'],
          'date_birth' => ['required','date'],
          'email' => ['required', 'string', 'email', 'max:100', 'unique:users'],
          'phone' => ['required', 'string', 'max:10'],
          'country_id' => ['required','integer', 'max:3'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data) {
        return User::create([
          'username' => $data['username'],
          'name' => $data['name'],
          'surnames' => $data['surnames'],
          //Everytime a user is created, the role is 'client'.
          'role_id' => '2',
          'user_photo' => $data['user_photo'],
          'password' => Hash::make($data['password']),
          'date_birth' => $data['date_birth'],
          'email' => $data['email'],
          'phone' => $data['phone'],
          'country_id' => $data['country_id'],
        ]);
    }
}
