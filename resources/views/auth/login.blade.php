@extends('layouts.menu')

@section('content')

<br><br><br><br><br><br><br><br><br><br><br><br>
<div class="text-center dropdown-content-login divLoginDiv">
  <h1 id="titleLogin">LOGIN</h1>
  <div class="login-form-1">
    <form method="POST" action="{{ route('login') }}" class="text-left">
      @csrf
      <div class="login-form-main-message"></div>
      <div class="main-login-form">
        <div class="login-group">
          <div class="form-group">
            <label for="email" class="sr-only">E-Mail</label>
            <input type="email" class="form-control @error('email') is-invalid @enderror"
            required name="email" placeholder="E-Mail"
            value="{{old('email')}}" id="email" autofocus>
            @error('email')
            <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
            </span>
            @enderror
          </div>
          <div class="form-group">
            <label for="password" class="sr-only">Password</label>
            <input type="password" class="form-control @error('password') is-invalid @enderror"
            id="password" required name="password" placeholder="Password">
            @error('password')
            <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
            </span>
            @enderror
          </div>
          <div class="form-group login-group-checkbox">
            <input type="checkbox" id="rememberLogin" name="rememberLogin"
            {{ old('remember') ? 'checked' : '' }}>
            <label for="rememberLogin">Remember</label>
          </div>
        </div>
        <button type="submit" class="login-button"><i class="fa fa-chevron-right"></i></button>
      </div><br>
      <div class="etc-login-form">
        <p>New user?<a class="styledA" href="{{route('register')}}"> Create new account</a></p>
      </div>
    </form>
  </div>
</div>
@endsection
