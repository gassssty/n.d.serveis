<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//To control the Auth methods.
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller {
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct() {
    $this->middleware('auth');
  }

  /**
  * Show the application dashboard.
  *
  * @return \Illuminate\Contracts\Support\Renderable
  */
  public function index() {
    //Taking the User Authenticated.
    $user = Auth::user();

    //Returning the User Authenticated.
    return view('home', compact('user'));
  }
}
