@extends('layouts.menu')

@section('content')

<br><br><br><br><br><br><br><br><br><br><br><br>
<div class="text-center dropdown-content-login divLoginDiv">
  <h1 id="titleLogin">REGISTER</h1>
  <div class="login-form-1">
    <form method="POST" action="{{ route('register') }}" class="text-left">
      @csrf
      <div class="login-form-main-message"></div>
      <div class="main-login-form">
        <div class="login-group">
          <!-- Username -->
          <div class="form-group">
            <input id="username" placeholder="Username" type="text" class="form-control" name="username" value="{{ old('username') }}" required autocomplete="username">
          </div>

          <!-- Name -->
          <div class="form-group">
            <input id="name" placeholder="Name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
            @error('name')
            <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
            </span>
            @enderror
          </div>

          <!-- Surnames -->
          <div class="form-group">
            <input id="surnames" placeholder="Surnames" type="text" class="form-control @error('surnames') is-invalid @enderror" name="surnames" value="{{ old('surnames') }}" required autocomplete="surnames">
            @error('surnames')
            <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
            </span>
            @enderror
          </div>

          <!-- E-Mail -->
          <div class="form-group">
            <input id="email" placeholder="E-Mail" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
            @error('email')
            <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
            </span>
            @enderror
          </div>

          <!-- Password -->
          <div class="form-group">
            <input id="password" placeholder="Password" type="password" class="form-control" name="password" required autocomplete="new-password">
            @error('password')
            <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
            </span>
            @enderror
          </div>

          <!-- Confirm Password -->
          <div class="form-group">
            <input id="password-confirm" placeholder="Confirm Password" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
          </div>

          <!-- Date of Birthday -->
          <div class="form-group">
            <input id="date_birth" placeholder="Date of Birthday"  type="date" class="form-control" name="date_birth" required autocomplete="date_birth">
          </div>

          <!-- Phone Number -->
          <div class="form-group">
            <input id="phone" placeholder="Phone Number" type="number" class="form-control"  name="phone" required autocomplete="phone">
          </div>

          <!-- Photo User -->
          <div class="form-group">
            <input readonly type="text" class="form-control" id="photo" name="photo" placeholder="Photo">
            <input id="user_photo" type="file" class="form-control" name="user_photo">
          </div>

          <!-- Country -->
          <div class="form-group">
            <select class="form-control" id="country_id" name="country_id" required autocomplete="country_id">
              <option value="1">Inglaterra</option>
              <option value="2">Espanya</option>
              <option value="3">Argentina</option>
              <option value="4">Francia</option>
              <option value="5">Colombia</option>
            </select>
          </div>

          <!-- Accept Terms -->
          <div class="form-group login-group-checkbox">
            <input type="checkbox" class="" id="reg_agree" name="reg_agree">
            <label for="reg_agree">I agree with <a href="#">terms</a></label>
          </div>
        </div>
        <button type="submit" class="login-button"><i class="fa fa-chevron-right"></i></button>
      </div>
      <div class="etc-login-form">
        <p>Already have an account? <a class="styledA" href="{{route('login')}}">Login Here</a></p>
      </div>
    </form>
  </div>
</div>
@endsection
